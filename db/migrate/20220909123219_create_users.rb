class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :address, limit: 255
      t.string :phone, limit: 12
      t.integer :gender, null: false
      t.timestamps
    end
  end
end
