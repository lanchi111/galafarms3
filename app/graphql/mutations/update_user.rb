module Mutations
  class UpdateUser < BaseMutation
    # arguments passed to the `resolve` method
    argument :id, Integer, required: true
    argument :first_name, String, required: false
    argument :last_name, String, required: false
    argument :gender, Integer, required: false
    argument :address, String, required: false, validates: { length: { maximum: 30 } }
    argument :phone, String, required: false
    argument :password, String, required: false
    argument :current_password, String, required: false

    # return type from the mutation
    type Types::UserType

    def resolve(id:, **attributes )
      User.find(id).tap do |user|
        user.update!(attributes)
      end
    end
  end
end