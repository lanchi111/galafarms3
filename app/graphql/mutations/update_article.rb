module Mutations
  class UpdateArticle < BaseMutation
    # arguments passed to the `resolve` method
    argument :id, Integer, required: true
    argument :title, String, required: false
    argument :body, String, required: false

    # return type from the mutation
    type Types::ArticleType

    def resolve(id:, **attributes )
      Article.find(id).tap do |article|
        article.update!(attributes)
      end
    end
  end
end