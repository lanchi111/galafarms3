module Mutations
  class CreateArticle < BaseMutation
    # arguments passed to the `resolve` method
    argument :title, String, required: true
    argument :body, String, required: true
    argument :user_id, Integer, required:true

    # return type from the mutation
    type Types::ArticleType

    def resolve(user_id:, **attributes )
      User.find(user_id).articles.create!(attributes)
    end
  end
  end