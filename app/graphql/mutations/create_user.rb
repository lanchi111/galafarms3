module Mutations
  class CreateUser < BaseMutation
    # arguments passed to the `resolve` method
    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :gender, Integer, required: true
    argument :email, String, required:true
    argument :password, String, required:true

    # return type from the mutation
    type Types::UserType

    def resolve(**attributes )
      User.create!(attributes)
    end
  end
end