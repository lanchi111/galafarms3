module Mutations
  class DestroyArticle < BaseMutation
    # arguments passed to the `resolve` method
    argument :id, Integer, required: true

    # return type from the mutation
    type Types::ArticleType

    def resolve(id:)
      Article.find(id).destroy
    end
  end
end