module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :all_articles, [Types::ArticleType], null: false,
      description: "Return all articles"

    def all_articles
      Article.all
    end

    field :article, ArticleType, "Find a article by ID" do
      argument :id, ID
    end
  
    # Then provide an implementation:
    def article(id:)
      Article.find(id)
    end

    field :user_articles,[Types::ArticleType], "Find all user article by userId" do
      argument :user_id, Integer, required:true
    end

    def user_articles(user_id:)
      User.find_by(id: user_id).try(:articles)
    end
  end
end
