class UserSerializer < ActiveModel::Serializer
  has_many :articles
  attributes :id, :first_name, :last_name, :email, :address, :articles

  def articles
    self.object.articles.map do |article|
      { title: article.title, body: article.body }
    end
  end
end
