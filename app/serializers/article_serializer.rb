class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :owner

  def owner
    { owner_id: self.object.user.id, owner_name: self.object.user.first_name }
  end
end
