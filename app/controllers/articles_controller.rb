class ArticlesController < ApplicationController
  # before_action :authenticate
  before_action :authenticate_user!
  before_action :set_user
  before_action :verify_user, only: %i[show edit destroy]

  def index
    @articles = current_user.articles
    respond_to do |format|
      format.html
      format.json { render json: current_user.articles, status: 200 }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @article.to_json, status: 200 }
    end
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render json: @article.to_json, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  def update
    @article = Article.find(params[:id])
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render json: @article.to_json, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { render json: "Deleted Successfully", status: :ok }
    end
  end

  private

  def set_user
    @current_user = current_user
  end

  def verify_user
    @article = Article.find_by(id: params[:id])
    if @article.try(:user_id) != current_user.id
      respond_to do |format|
        format.html { redirect_to articles_url, notice: 'You are not authorized' } 
        format.json { render json: "You are not authorized", status: :ok } 
      end
    end
  end

  def article_params
    params.require(:article).permit(:user_id, :title, :body, :image)
  end
end
