class RegistrationsController < Devise::RegistrationsController
  before_action :update_allowed_parameters, only: %i[update]
  respond_to :json

  # GET /resource/sign_up (/users/sign_up)
  def new
    super
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    respond_to do |format|
      if resource.save
        format.html { redirect_to '/', notice: 'User is successfully created.' }
        format.json { render json: resource, status: 200 }
      else
        format.html { render :new }
        format.json { render json: resource.errors, status: :unprocessable_entity }
      end
     end 
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super do |user|
      respond_to do |format|
        if user.errors.present?
          format.html { return render :new , notice: user.errors}
          format.json { return render json: user.errors, status: :unprocessable_entity }
        else
          format.html 
          format.json { return render json: user, status: 200 }
        end
      end
    end
  end

  # DELETE /resource
  def destroy
    current_user.destroy
    render json: "User Account Deleted Successfully", status: :ok
  end

  protected

  def update_allowed_parameters
    devise_parameter_sanitizer.permit(:account_update) do |u|
      u.permit(:first_name, :last_name, :gender, :address, :phone, :password, :current_password)
    end
  end

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :gender, :email, :password)
  end
end
