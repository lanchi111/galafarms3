class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  def authenticate
    return redirect_to new_user_session_path, notice: 'Please login to access.' unless user_signed_in?
    
    authenticate_or_request_with_http_basic do |email, password|
      user = User.find_by(email: email)
      !user.nil? && user.valid_password?(password)
    end
    warden.custom_failure! if performed?
  end
end
