class UserWelcomeJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    # Do something later
    @user = User.find_by(id: user_id)
    return unless @user

    UserMailer.with(user: @user).welcome_email.deliver_later
  end
end
