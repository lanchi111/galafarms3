class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :address, length: { maximum: 30 }

  has_many :articles, dependent: :destroy
  after_commit :user_welcome, on: :create

  enum gender: {
    male: 0,
    female: 1,
    other: 3
  }

  def user_welcome
    UserWelcomeJob.perform_later(id)
  end

  private

  def set_defaults
    self.gender ||= :other
  end
end
