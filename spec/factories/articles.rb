FactoryBot.define do
  factory :article do
    title { "Article Title" }
    body  { "Article body" }
  end
end
