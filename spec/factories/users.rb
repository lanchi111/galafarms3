FactoryBot.define do
  factory :user do
    first_name { "Lanchi" }
    last_name  { "Agrawal" }
    email { "pp@gmail.com" }
    password { "123456" }
    gender { 1 }
  end
end
