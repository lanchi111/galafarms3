require 'rails_helper'

module Mutations
  module Articles
    RSpec.describe DestroyArticle, type: :request do
      describe 'resolve' do
        let!(:user) { create(:user) }
        let!(:article) { create(:article, user: user) }

        it 'removes a article' do
          expect do
            post '/graphql', params: { query: query(id: article.id) }
          end.to change { Article.count }.by(-1)
        end

        it 'returns a article' do
          post '/graphql', params: { query: query(id: article.id) }
          json = JSON.parse(response.body)
          data = json['data']['destroyArticle']

          expect(data).to include(
            'id' => be_present,
            'title' => 'Article Title',
            'body' => 'Article body',
            'userId' => user.id
          )
        end
      end

      def query(id:)
        <<~GQL
          mutation {
            destroyArticle(input: {
              id: #{id}
            }) {
              id
              title
              body
              userId
            }
          }
        GQL
      end
    end
  end
end
