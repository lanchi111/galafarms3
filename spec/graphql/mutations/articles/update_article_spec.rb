require 'rails_helper'

module Mutations
  module Articles
    RSpec.describe UpdateArticle, type: :request do
      describe 'resolve' do
        let!(:user) { create(:user) }
        let!(:article) { create(:article, user: user) }

        it 'update a article' do
          post '/graphql', params: { query: query(id: article.id) }
          json = JSON.parse(response.body)
          data = json['data']['updateArticle']

          expect(data).to include(
            'id' => article.id.to_s,
            'title' => 'This is title',
            'body' => 'This is body',
            'userId' => user.id
          )
        end
      end

      def query(id:)
        <<~GQL
          mutation {
            updateArticle(input: {
              id: #{id}
              title: "This is title"
              body:  "This is body"
            }) {
              id
              title
              body
              userId
            }
          }
        GQL
      end
    end
  end
end
