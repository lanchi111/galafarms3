require 'rails_helper'

module Mutations
  module Articles
    RSpec.describe CreateArticle, type: :request do
      describe '.resolve' do
        let!(:user) { create(:user) }

        it 'create a article' do
          post '/graphql', params: { query: query(user_id: user.id) }
          json = JSON.parse(response.body)
          data = json['data']['createArticle']

          expect(data).to include(
            'id' => be_present,
            'title' => 'Article Title',
            'body' => 'Article Body',
            'user' => { 'id' => user.id.to_s }
          )
        end
      end

      def query(user_id:)
        <<~GQL
          mutation {
            createArticle(input: {
              userId: #{user_id}#{' '}
              title: "Article Title"
              body: "Article Body"
            }) {
              id
              title
              body
              user {
                id
              }
            }
          }
        GQL
      end
    end
  end
end
