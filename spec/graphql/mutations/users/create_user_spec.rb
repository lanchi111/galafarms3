require 'rails_helper'

module Mutations
  module Users
    RSpec.describe CreateUser, type: :request do
      describe '.resolve' do
        
        it 'create a user' do
          post '/graphql', params: { query: query }
          json = JSON.parse(response.body)
          data = json['data']['createUser']

          expect(data).to include(
            'id' => be_present,
            'firstName' => 'John',
            'lastName' => 'Dubey',
            'gender' => 0,
            'email' => 'john2@example.com',
          )
        end

        it 'fails when first name is not present' do
          post '/graphql', params: { query: query2 }
          json = JSON.parse(response.body)
          error = json['errors'][0]

          expect(error).to include(
            'message' => "Argument 'firstName' on InputObject 'CreateUserInput' is required. Expected type String!"
          )
        end
      end

      def query
        <<~GQL
         mutation {
          createUser(input: {
            firstName: "John",
            lastName: "Dubey",
            email: "john2@example.com",
            password: "123456"
            gender: 0,
          }) {
            id
            firstName
            lastName
            gender
            email
          }
         }
        GQL
      end

      def query2
        <<~GQL
         mutation {
          createUser(input: {
            lastName: "Dubey",
            email: "john2@example.com",
            password: "123456"
            gender: 0,
          }) {
            id
            firstName
            lastName
            gender
            email
          }
         }
        GQL
      end
    end
  end
end
