require 'rails_helper'

module Mutations
  module Users
    RSpec.describe UpdateUser, type: :request do
      describe 'resolve' do
        let!(:user) { create(:user) }

        it 'update a user' do
          post '/graphql', params: { query: query(id: user.id) }
          json = JSON.parse(response.body)
          data = json['data']['updateUser']

          expect(data).to include(
            'firstName' => 'John',
            'lastName' => 'Agrawal',
            'gender' => 0,
            'address' => 'Master colony',
            'phone' => '8877888899',
            'email' => user.email
          )
        end

        it 'fails when address is more than 30 characters' do
          post '/graphql', params: { query: query2(id: user.id) }
          json = JSON.parse(response.body)
          error = json['errors'][0]
          
          expect(error).to include(
            'message' => "address is too long (maximum is 30)"
          )
        end
      end

      def query(id:)
        <<~GQL
        mutation {
          updateUser(input: {
            id: #{id},
            firstName: "John",
            lastName: "Agrawal",
            gender: 0,
            address: "Master colony",
            phone: "8877888899",
          }) {
            firstName
            lastName
            gender
            address
            phone
            email
          }
          }
        GQL
      end

      def query2(id:)
        <<~GQL
        mutation {
          updateUser(input: {
            id: #{id},
            firstName: "John",
            lastName: "Agrawal",
            gender: 0,
            address: "Above Jolly Dwarfjunior School, F/73,1st Floor, Mira Mahal, Off Link Road, ,obe, Andheri (west)",
            phone: "8877888899",
          }) {
            firstName
            lastName
            gender
            phone
            email
          }
          }
        GQL
      end
    end
  end
end
