require 'rails_helper'

RSpec.describe Article, type: :model do
  let!(:user)   { create(:user) }
  subject { Article.new(title: "Article title", body: "Article Body", user: user) }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a title" do
    subject.title = nil
    expect(subject).to_not be_valid
  end
  
  it "is not valid without a body" do
    subject.body = nil
    expect(subject).to_not be_valid
  end
end
