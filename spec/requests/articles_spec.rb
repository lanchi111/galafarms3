require 'rails_helper'

RSpec.describe "/articles", type: :request do
  before do
    @user = create(:user)
    @headers = {
      "ACCEPT" => "application/json",
      "HTTP_AUTHORIZATION" => ActionController::HttpAuthentication::Basic.encode_credentials(@user.email, @user.password)
    }
  end

  let!(:article)   { create(:article, user: @user) }
  let!(:article1)  { create(:article, title: "lanchi", body: "This is lanchi article body", user: @user) }

  it 'get all articles' do
    get "/articles", headers: @headers
    expect(response).to have_http_status(:ok)
    expect(JSON.parse(response.body)).to eq([
                                              {
                                                "id" => article.id,
                                                "title" => "Article Title",
                                                "body" => "Article body",
                                                "owner" => {
                                                  "owner_id" => @user.id,
                                                  "owner_name" => "Lanchi"
                                                }
                                              },
                                              {
                                                "id" => article1.id,
                                                "title" => "lanchi",
                                                "body" => "This is lanchi article body",
                                                "owner" => {
                                                  "owner_id" => @user.id,
                                                  "owner_name" => "Lanchi"
                                                }
                                              }
                                            ])
  end

  it 'show article/article_id' do
    get "/articles/#{article.id}", headers: @headers
    json = JSON.parse(response.body).deep_symbolize_keys
    expect(json[:title]).to eq("Article Title")
    expect(json[:body]).to eq("Article body")
  end

  it 'create article/post' do
    post "/articles", params: {
      article: attributes_for(:article)
    }, headers: @headers
    expect(response).to have_http_status(:created)
    json = JSON.parse(response.body).deep_symbolize_keys
    expect(json[:title]).to eq("Article Title")
    expect(json[:body]).to eq("Article body")
  end

  it 'update article/article_id' do
    put "/articles/#{article.id}", params: {
      article: {
        title: "update title",
        body: "update body"
      }
    }, headers: @headers
    expect(response).to have_http_status(:ok)
    json = JSON.parse(response.body).deep_symbolize_keys
    expect(json[:title]).to eq("update title")
    expect(json[:body]).to eq("update body")
  end

  it 'delete article/article_id' do
    delete "/articles/#{article.id}", headers: @headers
    expect(response).to have_http_status(:ok)
    expect(response.body).to eq("Deleted Successfully")
  end
end
